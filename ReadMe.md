# Project Dating website
Joan Padolina
## Jokes on Me 

A dating app for the ones seeking for a partner. A good partner is who you share intimate things with but the most important is a good laugh together.

Obviously a good sense humor is where you can share weird jokes with eachother. Laugh together is stay together.

![Wireflow app](https://github.com/joanpadolina/Project_Tech/blob/master/documentatie/Jome-branding-sketch.png)

# True Love Through Laugh

## Wiki

Follow the progress on my [wiki](https://github.com/joanpadolina/Project_Tech.wiki.git).

# Interested? Clone the following on your terminal:

## Some ingredients before cloning

* NPM // install packages
* Templating EJS  // for a dynamic page
* Express // routing through pages
* Node.js // local database
* MongoDB Compass // database 
* Code Editor (I use Atom) // for creating magic

## Instal 

```
git clone https://github.com/joanpadolina/Project_Tech
```

Install used npm
```
npm install
```
Run the application
```
npm run dev
```
Create an .ENV file
```
DB_HOST=localhost
DB_PORT= -any port like 3000-  
DB_NAME= -name your database- 
SESSION_SECRET= -secure session cookies-
```


# What's working
1. Insert form to MongoDB
1. Get info in a Template (dynamic info)
1. Login with existing account from MongoDB
1. Show full database (who has registered) on the browser


# MIT
[MIT](https://github.com/joanpadolina/Project_Tech/blob/master/LICENSE)
